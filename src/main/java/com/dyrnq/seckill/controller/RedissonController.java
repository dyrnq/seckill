package com.dyrnq.seckill.controller;

import lombok.RequiredArgsConstructor;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequiredArgsConstructor
public class RedissonController {

    final RedissonClient redissonClient;

    @ResponseBody
    @RequestMapping("atomicLong")
    public String atomicLong() {
        RAtomicLong atomicLong = redissonClient.getAtomicLong("myAtomicLong");
        long val = atomicLong.getAndIncrement();
        return String.valueOf(val);
    }

    @ResponseBody
    @RequestMapping("json")
    public Result<Map<String, Object>> json() {
        Map<String, Object> map = new HashMap<>();
        map.put("long", Long.MAX_VALUE);
        map.put("date", new Date());
        map.put("localDate", LocalDate.now());
        map.put("localDateTime", LocalDateTime.now());
        map.put("localTime", LocalTime.now());
        return Result.success(map);
    }
}
