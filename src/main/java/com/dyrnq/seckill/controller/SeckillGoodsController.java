package com.dyrnq.seckill.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dyrnq
 * @since 2023-12-06
 */
@Controller
@RequestMapping("/seckillGoods")
public class SeckillGoodsController {

}
