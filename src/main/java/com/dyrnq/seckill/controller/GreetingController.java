package com.dyrnq.seckill.controller;

import cn.hutool.core.date.DateUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;

import java.util.Date;

@Controller
@RequiredArgsConstructor
@EnableScheduling
public class GreetingController {

    private final SimpMessagingTemplate simpMessagingTemplate;
    @Scheduled(fixedDelay = 3000)
    public void boardCast(){
        this.simpMessagingTemplate.convertAndSend("/topic", "来自服务器广播消息: " + DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss.SSS"));
    }

    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public Greeting greeting(HelloMessage message) throws Exception {
        Thread.sleep(1000); // simulated delay
        return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
    }

}