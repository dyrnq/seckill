package com.dyrnq.seckill.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 抢红包记录表，抢一个红包插入一条记录 前端控制器
 * </p>
 *
 * @author dyrnq
 * @since 2024-02-16
 */
@Controller
@RequestMapping("/redPacketRecord")
public class RedPacketRecordController {

}
