package com.dyrnq.seckill.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
public class SessionController {

    @ResponseBody
    @RequestMapping("session")
    public Result<Map<String, Object>> json(HttpSession session) {
        Map<String, Object> map = new HashMap<>();
        map.put("long", Long.MAX_VALUE);
        map.put("date", new Date());
        map.put("localDate", LocalDate.now());
        map.put("localDateTime", LocalDateTime.now());
        map.put("localTime", LocalTime.now());
        if (session.getAttribute("json") == null) {
            session.setAttribute("json", map);
        }
        return Result.success((Map<String, Object>) session.getAttribute("json"));
    }
}
