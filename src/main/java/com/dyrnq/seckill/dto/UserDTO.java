package com.dyrnq.seckill.dto;

import lombok.Data;

@Data
public class UserDTO {
    private Integer id;
    private String userName;
    private String phone;
    private String head;
}
