package com.dyrnq.seckill.dto;

import lombok.Data;

@Data
public class LoginDTO {
    private String mobile;
    private String password;
}
