package com.dyrnq.seckill.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class SeckillGoodsDTO {
    /**
     * goods表字段
     */
    private String goodsName;
    private String goodsTitle;
    private String goodsImg;
    private String goodsDetail;
    private BigDecimal goodsPrice;
    private Integer goodsStock;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;


    /**
     * seckill_goods表字段
     */
    private Long id;
    private Long goodsId;
    private BigDecimal seckilPrice;
    private Integer stockCount;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
}
