package com.dyrnq.seckill.service;

import com.dyrnq.seckill.entity.RedPacketInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 红包信息表，新建一个红包插入一条记录 服务类
 * </p>
 *
 * @author dyrnq
 * @since 2024-02-16
 */
public interface IRedPacketInfoService extends IService<RedPacketInfo> {

}
