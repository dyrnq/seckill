package com.dyrnq.seckill.service;

import com.dyrnq.seckill.entity.Goods;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dyrnq
 * @since 2023-12-06
 */
public interface IGoodsService extends IService<Goods> {

}
