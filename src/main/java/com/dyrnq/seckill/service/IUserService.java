package com.dyrnq.seckill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dyrnq.seckill.dto.LoginDTO;
import com.dyrnq.seckill.dto.UserDTO;
import com.dyrnq.seckill.entity.User;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author dyrnq
 * @since 2023-12-06
 */
public interface IUserService extends IService<User> {
    UserDTO login(LoginDTO loginDTO);
}
