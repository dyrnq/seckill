package com.dyrnq.seckill.service;

import com.dyrnq.seckill.entity.RedPacketRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 抢红包记录表，抢一个红包插入一条记录 服务类
 * </p>
 *
 * @author dyrnq
 * @since 2024-02-16
 */
public interface IRedPacketRecordService extends IService<RedPacketRecord> {

}
