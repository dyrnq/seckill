package com.dyrnq.seckill.service.impl;

import com.dyrnq.seckill.entity.RedPacketRecord;
import com.dyrnq.seckill.mapper.RedPacketRecordMapper;
import com.dyrnq.seckill.service.IRedPacketRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 抢红包记录表，抢一个红包插入一条记录 服务实现类
 * </p>
 *
 * @author dyrnq
 * @since 2024-02-16
 */
@Service
public class RedPacketRecordServiceImpl extends ServiceImpl<RedPacketRecordMapper, RedPacketRecord> implements IRedPacketRecordService {

}
