package com.dyrnq.seckill.service.impl;

import com.dyrnq.seckill.entity.RedPacketInfo;
import com.dyrnq.seckill.mapper.RedPacketInfoMapper;
import com.dyrnq.seckill.service.IRedPacketInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 红包信息表，新建一个红包插入一条记录 服务实现类
 * </p>
 *
 * @author dyrnq
 * @since 2024-02-16
 */
@Service
public class RedPacketInfoServiceImpl extends ServiceImpl<RedPacketInfoMapper, RedPacketInfo> implements IRedPacketInfoService {

}
