package com.dyrnq.seckill.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dyrnq.seckill.entity.OrderInfo;
import com.dyrnq.seckill.mapper.OrderInfoMapper;
import com.dyrnq.seckill.service.IOrderInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author dyrnq
 * @since 2023-12-06
 */
@Service
public class OrderInfoServiceImpl extends ServiceImpl<OrderInfoMapper, OrderInfo> implements IOrderInfoService {

}
