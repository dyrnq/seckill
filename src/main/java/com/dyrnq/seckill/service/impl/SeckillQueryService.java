package com.dyrnq.seckill.service.impl;

import com.dyrnq.seckill.dto.SeckillGoodsDTO;
import com.dyrnq.seckill.entity.Goods;
import com.dyrnq.seckill.entity.SeckillGoods;
import com.dyrnq.seckill.mapper.SeckillGoodsJoinMapper;
import com.github.yulichang.wrapper.MPJLambdaWrapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SeckillQueryService {
    protected final SeckillGoodsJoinMapper seckillGoodsJoinMapper;

    /**
     * <a href="https://mybatisplusjoin.com/pages/quickstart/ksks.html">...</a>
     *
     * @return
     */
    public List<SeckillGoodsDTO> listSeckillGoodsDTO() {
        MPJLambdaWrapper<SeckillGoods> wrapper = new MPJLambdaWrapper<SeckillGoods>()
                .selectAll(SeckillGoods.class)
                .select(Goods::getGoodsDetail, Goods::getGoodsImg, Goods::getGoodsName, Goods::getGoodsPrice, Goods::getGoodsTitle, Goods::getCreateDate, Goods::getUpdateDate)
                .leftJoin(Goods.class, Goods::getId, SeckillGoods::getGoodsId);
        List<SeckillGoodsDTO> list = seckillGoodsJoinMapper.selectJoinList(SeckillGoodsDTO.class, wrapper);
        return list;
    }

    public SeckillGoodsDTO getSeckillGoodsDTO(Long id) {
        MPJLambdaWrapper<SeckillGoods> wrapper = new MPJLambdaWrapper<SeckillGoods>()
                .selectAll(SeckillGoods.class)
                .select(Goods::getGoodsDetail, Goods::getGoodsImg, Goods::getGoodsName, Goods::getGoodsPrice, Goods::getGoodsTitle, Goods::getCreateDate, Goods::getUpdateDate)
                .leftJoin(Goods.class, Goods::getId, SeckillGoods::getGoodsId).eq(SeckillGoods::getId, id);
        return seckillGoodsJoinMapper.selectJoinOne(SeckillGoodsDTO.class, wrapper);
    }
}
