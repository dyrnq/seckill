package com.dyrnq.seckill.service;

import com.dyrnq.seckill.entity.SeckillGoods;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dyrnq
 * @since 2023-12-06
 */
public interface ISeckillGoodsService extends IService<SeckillGoods> {

}
