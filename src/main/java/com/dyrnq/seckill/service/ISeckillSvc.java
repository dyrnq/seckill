package com.dyrnq.seckill.service;

public interface ISeckillSvc {
    /**
     * 秒杀（一人可以多单）
     *
     * @param goods_id
     * @param user_id
     * @return
     */
    long doSeckill(long goods_id, long user_id);

    /**
     * 秒杀一人一单
     *
     * @param goods_id
     * @param user_id
     * @return
     */
    long doSeckill_1user1order(long goods_id, long user_id);


    /**
     * 秒杀一人一单 （前置redis预减库存）
     *
     * @param goods_id
     * @param user_id
     * @return
     */
    long doSeckill_1user1order_redis(long goods_id, long user_id);

}
