package com.dyrnq.seckill.mq;

import lombok.Data;

@Data
public class MessageVo {
    long goodsId;
    long userId;
}
