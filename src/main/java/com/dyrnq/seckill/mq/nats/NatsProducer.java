package com.dyrnq.seckill.mq.nats;

import cn.hutool.json.JSONUtil;
import com.dyrnq.seckill.mq.MessageVo;
import com.dyrnq.seckill.mq.Producer;
import io.nats.client.Connection;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;

@Service
@RequiredArgsConstructor
@ConditionalOnProperty(value = "queue.impl", havingValue = "nats")
public class NatsProducer implements Producer {
    private final Connection natsConnection;

    @Override
    public void send(String destination, MessageVo message) {
        String msg = JSONUtil.toJsonStr(message);
        natsConnection.publish(destination, msg.getBytes(Charset.defaultCharset()));
    }
}
