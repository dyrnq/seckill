package com.dyrnq.seckill.mq.hazelcast;

import com.dyrnq.seckill.mq.MessageVo;
import com.dyrnq.seckill.mq.Producer;
import com.hazelcast.collection.IQueue;
import com.hazelcast.core.HazelcastInstance;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@ConditionalOnProperty(value = "queue.impl", havingValue = "hazelcast")
public class HazelcastProducer implements Producer {
    private final HazelcastInstance hazelcastInstance;

    @Override
    public void send(String destination, MessageVo message) {
        IQueue<MessageVo> queue = hazelcastInstance.getQueue(Producer.QUEUE_NAME);
        queue.offer(message);
//        Ringbuffer<MessageVo> buffer = hazelcastInstance.getRingbuffer(Producer.QUEUE_NAME);
//        buffer.add(message);
    }
}
