package com.dyrnq.seckill.mq.hazelcast;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.hazelcast.HazelcastAutoConfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnExpression("! T(org.apache.commons.lang3.StringUtils).equalsIgnoreCase('hazelcast','${queue.impl}')")
@EnableAutoConfiguration(exclude = HazelcastAutoConfiguration.class)
public class HazelcastConditional {

}
