package com.dyrnq.seckill.mq.disruptor;

import com.dyrnq.seckill.mq.MessageVo;
import com.dyrnq.seckill.mq.Producer;
import com.lmax.disruptor.RingBuffer;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DisruptorProducer implements Producer {
    private final RingBuffer<MessageVo> ringBuffer;

    @Override
    public void send(String destination, MessageVo message) {
        long sequence = ringBuffer.next();
        try {
            MessageVo item = ringBuffer.get(sequence);
            item.setUserId(message.getUserId());
            item.setGoodsId(message.getGoodsId());
        } finally {
            ringBuffer.publish(sequence);
        }
    }
}
