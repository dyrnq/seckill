package com.dyrnq.seckill.mq.disruptor;


import com.dyrnq.seckill.mq.MessageVo;
import com.dyrnq.seckill.service.impl.SeckillOrderService;
import com.lmax.disruptor.WorkHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@RequiredArgsConstructor
@Slf4j
public class DisruptorConsumer implements WorkHandler<MessageVo> {

    private final SeckillOrderService seckillOrderService;

    @Override
    public void onEvent(MessageVo msg) throws Exception {
        log.info("Message GoodsId {} , UserId {}", msg.getGoodsId(), msg.getUserId());
        long goods_id = msg.getGoodsId();
        long user_id = msg.getUserId();
        seckillOrderService.insert(goods_id, user_id);
    }
}
