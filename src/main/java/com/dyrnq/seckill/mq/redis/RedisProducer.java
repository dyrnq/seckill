package com.dyrnq.seckill.mq.redis;

import com.dyrnq.seckill.mq.MessageVo;
import com.dyrnq.seckill.mq.Producer;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@ConditionalOnProperty(value = "queue.impl", havingValue = "redis")
public class RedisProducer implements Producer {
    @Override
    public void send(String destination, MessageVo message) {
        // 在lua脚本扣减库存中一并处理了（XADD命令放到redis stream 队列中）
    }
}
