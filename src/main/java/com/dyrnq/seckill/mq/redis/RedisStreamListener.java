package com.dyrnq.seckill.mq.redis;

import com.dyrnq.seckill.mq.MessageVo;
import com.dyrnq.seckill.service.impl.SeckillOrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.redis.connection.stream.ObjectRecord;
import org.springframework.data.redis.stream.StreamListener;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Slf4j
@Component("redisStreamListener")
@ConditionalOnProperty(value = "queue.impl", havingValue = "redis")
public class RedisStreamListener implements StreamListener<String, ObjectRecord<String, MessageVo>> {


    private final SeckillOrderService seckillOrderService;

    @Override
    public void onMessage(ObjectRecord<String, MessageVo> message) {
        MessageVo msg = message.getValue();
        log.info("Message GoodsId {} , UserId {} | Consumed {} | ID {}", msg.getGoodsId(), msg.getUserId(), message.getStream(), message.getId());
        long goods_id = msg.getGoodsId();
        long user_id = msg.getUserId();
        seckillOrderService.insert(goods_id, user_id);
    }
}
