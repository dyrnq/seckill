package com.dyrnq.seckill.mq;

public interface Producer {
    String QUEUE_NAME="seckill";
    void send(String destination, MessageVo message);
}
