package com.dyrnq.seckill.mq.rocketmq;

import com.dyrnq.seckill.mq.MessageVo;
import com.dyrnq.seckill.service.impl.SeckillOrderService;
import lombok.RequiredArgsConstructor;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;


@RocketMQMessageListener(topic = "seckill", consumerGroup = "seckill-group")
@Service
@RequiredArgsConstructor
@ConditionalOnProperty(value = "queue.impl", havingValue = "rocketmq")
public class RocketmqConsumer implements RocketMQListener<MessageVo> {
    private final SeckillOrderService seckillOrderService;

    @Override
    public void onMessage(MessageVo msg) {
        long goods_id = msg.getGoodsId();
        long user_id = msg.getUserId();
        seckillOrderService.insert(goods_id, user_id);
    }

}
