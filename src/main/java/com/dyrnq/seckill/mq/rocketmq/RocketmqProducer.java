package com.dyrnq.seckill.mq.rocketmq;

import com.dyrnq.seckill.mq.MessageVo;
import com.dyrnq.seckill.mq.Producer;
import lombok.RequiredArgsConstructor;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@ConditionalOnProperty(value = "queue.impl", havingValue = "rocketmq")
public class RocketmqProducer implements Producer {
    private final RocketMQTemplate rocketMQTemplate;

    /**
     * @param destination
     * @param message
     */
    public void send(String destination, MessageVo message) {
        rocketMQTemplate.convertAndSend(destination, message);
    }
}
