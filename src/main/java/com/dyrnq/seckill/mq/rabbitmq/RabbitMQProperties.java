package com.dyrnq.seckill.mq.rabbitmq;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Data
@Configuration
@ConfigurationProperties(prefix = "rabbitmq")
public class RabbitMQProperties {
    private String nodes;
    private String connectionName;
    private String virtualHost;
    private String userName;
    private String password;
    private String exchangeName;
}
