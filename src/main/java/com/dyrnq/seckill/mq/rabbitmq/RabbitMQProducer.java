package com.dyrnq.seckill.mq.rabbitmq;

import com.dyrnq.seckill.mq.MessageVo;
import com.dyrnq.seckill.mq.Producer;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@ConditionalOnProperty(value = "queue.impl", havingValue = "rabbitmq")
public class RabbitMQProducer implements Producer {
    private final RabbitTemplate rabbitTemplate;
    private final RabbitMQProperties rabbitMqProperties;

    public RabbitMQProducer(@Qualifier(RabbitConstants.RABBIT_SEND_TEMPLATE) RabbitTemplate rabbitTemplate, RabbitMQProperties rabbitMqProperties) {
        this.rabbitTemplate = rabbitTemplate;
        this.rabbitMqProperties = rabbitMqProperties;
    }

    @Override
    public void send(String destination, MessageVo message) {
        CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString());
        rabbitTemplate.convertAndSend(rabbitMqProperties.getExchangeName(), destination, message, correlationData);
    }
}
