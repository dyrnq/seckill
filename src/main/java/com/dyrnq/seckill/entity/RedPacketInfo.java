package com.dyrnq.seckill.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 红包信息表，新建一个红包插入一条记录
 * </p>
 *
 * @author dyrnq
 * @since 2024-02-16
 */
@TableName("red_packet_info")
public class RedPacketInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 红包id
     */
    private Long redPacketId;

    /**
     * 红包总个数
     */
    private Integer totalPacket;

    /**
     * 红包总金额，单位分
     */
    private Integer totalAmount;

    /**
     * 剩余红包金额，单位分
     */
    private Integer remainingAmount;

    /**
     * 剩余红包个数
     */
    private Integer remainingPacket;

    /**
     * 创建建红包的用户标识
     */
    private Long uid;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 版本号用于乐观锁
     */
    private Integer version;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getRedPacketId() {
        return redPacketId;
    }

    public void setRedPacketId(Long redPacketId) {
        this.redPacketId = redPacketId;
    }

    public Integer getTotalPacket() {
        return totalPacket;
    }

    public void setTotalPacket(Integer totalPacket) {
        this.totalPacket = totalPacket;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(Integer remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public Integer getRemainingPacket() {
        return remainingPacket;
    }

    public void setRemainingPacket(Integer remainingPacket) {
        this.remainingPacket = remainingPacket;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "RedPacketInfo{" +
            "id = " + id +
            ", redPacketId = " + redPacketId +
            ", totalPacket = " + totalPacket +
            ", totalAmount = " + totalAmount +
            ", remainingAmount = " + remainingAmount +
            ", remainingPacket = " + remainingPacket +
            ", uid = " + uid +
            ", createTime = " + createTime +
            ", updateTime = " + updateTime +
            ", version = " + version +
        "}";
    }
}
