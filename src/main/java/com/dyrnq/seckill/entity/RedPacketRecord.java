package com.dyrnq.seckill.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 抢红包记录表，抢一个红包插入一条记录
 * </p>
 *
 * @author dyrnq
 * @since 2024-02-16
 */
@TableName("red_packet_record")
public class RedPacketRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 抢到红包的金额，单位分
     */
    private Integer amount;

    /**
     * 抢到红包的用户的用户名
     */
    private String nickName;

    /**
     * 抢到红包的用户的头像
     */
    private String imgUrl;

    /**
     * 抢到红包用户的用户标识
     */
    private Long uid;

    /**
     * 红包id
     */
    private Long redPacketId;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getRedPacketId() {
        return redPacketId;
    }

    public void setRedPacketId(Long redPacketId) {
        this.redPacketId = redPacketId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "RedPacketRecord{" +
            "id = " + id +
            ", amount = " + amount +
            ", nickName = " + nickName +
            ", imgUrl = " + imgUrl +
            ", uid = " + uid +
            ", redPacketId = " + redPacketId +
            ", createTime = " + createTime +
            ", updateTime = " + updateTime +
        "}";
    }
}
