package com.dyrnq.seckill.mapper;

import com.dyrnq.seckill.entity.SeckillGoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dyrnq
 * @since 2023-12-06
 */
public interface SeckillGoodsMapper extends BaseMapper<SeckillGoods> {

    @Update("UPDATE seckill_goods SET stock_count = stock_count - #{count} WHERE id = #{id} AND stock_count = #{oldStockCount}")
    int updateStockCount(long id, int count, int oldStockCount);

    @Update("UPDATE seckill_goods SET stock_count = stock_count - #{count} WHERE id = #{id} AND stock_count >0")
    int updateStockCountById(long id, int count);

    @Select("SELECT * FROM seckill_goods WHERE id = #{id} for update")
    SeckillGoods selectByIdPessimistic(long id);

    @Update("UPDATE seckill_goods SET stock_count = 0 WHERE id = #{id} ")
    int updateStockCountZero(long id);

}
