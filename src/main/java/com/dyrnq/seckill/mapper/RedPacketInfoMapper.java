package com.dyrnq.seckill.mapper;

import com.dyrnq.seckill.entity.RedPacketInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 * 红包信息表，新建一个红包插入一条记录 Mapper 接口
 * </p>
 *
 * @author dyrnq
 * @since 2024-02-16
 */
public interface RedPacketInfoMapper extends BaseMapper<RedPacketInfo> {
    @Select("SELECT * FROM red_packet_info WHERE red_packet_id = #{redPacketId}")
    RedPacketInfo selectByRedPacketId(Long redPacketId);

    @Update("UPDATE red_packet_info SET remaining_packet = remaining_packet - 1 , remaining_amount = remaining_amount - #{amount} , version = version + 1 WHERE id = #{id} AND version = #{version}")
    int decrById(long id, int amount, int version);
}
