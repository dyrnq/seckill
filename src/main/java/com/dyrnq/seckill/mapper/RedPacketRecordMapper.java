package com.dyrnq.seckill.mapper;

import com.dyrnq.seckill.entity.RedPacketRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 抢红包记录表，抢一个红包插入一条记录 Mapper 接口
 * </p>
 *
 * @author dyrnq
 * @since 2024-02-16
 */
public interface RedPacketRecordMapper extends BaseMapper<RedPacketRecord> {

}
