package com.dyrnq.seckill.mapper;

import com.dyrnq.seckill.entity.SeckillOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dyrnq
 * @since 2023-12-06
 */
public interface SeckillOrderMapper extends BaseMapper<SeckillOrder> {

}
