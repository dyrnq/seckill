package com.dyrnq.seckill.mapper;

import com.dyrnq.seckill.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dyrnq
 * @since 2023-12-06
 */
public interface UserMapper extends BaseMapper<User> {
    @Select("SELECT * FROM user WHERE phone = #{phone}")
    User selectByPhone(String phone);
}
