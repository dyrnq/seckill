package com.dyrnq.seckill.mapper;

import com.github.yulichang.base.MPJBaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.dyrnq.seckill.entity.SeckillGoods;

@Mapper
public interface SeckillGoodsJoinMapper extends MPJBaseMapper<SeckillGoods> {

}
