local streamKey = ARGV[1]

local group = ARGV[2]

local id = ARGV[3]

-- EXISTS myStream
if(tonumber(redis.call('EXISTS', streamKey)) <= 0) then
-- (error) ERR The XGROUP subcommand requires the key to exist. Note that for CREATE you may want to use the MKSTREAM option to create an empty stream automatically.
    redis.call('XGROUP','create',streamKey,group,id,'MKSTREAM')
    -- xgroup create myStream group1 $ MKSTREAM
end
return 0