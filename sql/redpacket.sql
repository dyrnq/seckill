DROP TABLE IF EXISTS `red_packet_info`;
CREATE TABLE `red_packet_info` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `red_packet_id` bigint(20) NOT NULL DEFAULT  0 COMMENT '红包id',
    `total_packet` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '红包总个数',
    `total_amount` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '红包总金额，单位分',
    `remaining_amount` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '剩余红包金额，单位分',
    `remaining_packet` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '剩余红包个数',
    `uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '创建建红包的用户标识',
    `create_time` DATETIME (6) NOT NULL DEFAULT CURRENT_TIMESTAMP (6) ON UPDATE CURRENT_TIMESTAMP (6) COMMENT '创建时间',
    `update_time` DATETIME (6) NOT NULL DEFAULT CURRENT_TIMESTAMP (6) ON UPDATE CURRENT_TIMESTAMP (6) COMMENT '更新时间',
    `version` int(11) NOT NULL DEFAULT 0 COMMENT '版本号用于乐观锁',
    PRIMARY KEY (`id`),
    UNIQUE KEY `red_packet_id` (`red_packet_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='红包信息表，新建一个红包插入一条记录';

-- DATETIME()没有毫秒值，DATETIME(3)毫秒值有3位，DATETIME(6)有6位，恰好对应建表时datetime后缀中的数字。



DROP TABLE IF EXISTS `red_packet_record`;
CREATE TABLE `red_packet_record` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `amount` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '抢到红包的金额，单位分',
    `nick_name` varchar(32) NOT NULL DEFAULT '0' COMMENT '抢到红包的用户的用户名',
    `img_url` varchar(255) NOT NULL DEFAULT '0' COMMENT '抢到红包的用户的头像',
    `uid` bigint(20) NOT NULL DEFAULT '0' COMMENT '抢到红包用户的用户标识',
    `red_packet_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '红包id',
    `create_time` DATETIME (6) NOT NULL DEFAULT CURRENT_TIMESTAMP (6) ON UPDATE CURRENT_TIMESTAMP (6) COMMENT '创建时间',
    `update_time` DATETIME (6) NOT NULL DEFAULT CURRENT_TIMESTAMP (6) ON UPDATE CURRENT_TIMESTAMP (6) COMMENT '更新时间',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uid_red_packet_id` (`uid`,`red_packet_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='抢红包记录表，抢一个红包插入一条记录';

