<!-- TOC -->

- [requirements](#requirements)
- [start vm](#start-vm)
- [build](#build)
- [run](#run)
- [sql](#sql)
- [api](#api)
- [tables](#tables)
  - [goods](#goods)
  - [seckill_goods](#seckill_goods)
  - [order_info](#order_info)
  - [seckill_order](#seckill_order)
  - [user](#user)
  - [red_packet_info](#red_packet_info)
  - [red_packet_record](#red_packet_record)
- [ref](#ref)

<!-- /TOC -->

## requirements

- VirtualBox
- Vagrant


## start vm

```bash
## 克隆代码
git clone git@gitee.com:dyrnq/seckill.git
cd seckill

## 启动虚拟机
vagrant up seckill

## ssh登录虚拟机
vagrant ssh seckill

# 如果报错 vagrant@127.0.0.1: Permission denied (publickey). 
# 执行 set VAGRANT_PREFER_SYSTEM_BIN=0

## 切换用户为root，root密码为vagrant
su --login root

## 安装必备软件
cd /vagrant && bash ./install.sh
```



## build

```bash
./mvnw clean package -Dmaven.test.skip=true -s ./settings.xml
```

## run

### 选项一(运行main函数)

直接运行 `com.dyrnq.seckill.SeckillApplication` 的`main`函数

### 选项二(运行spring-boot:run)

```bash
./mvnw spring-boot:run
```

### 选项三(打包后运行java -jar)

```bash
javar \
-server \
-Djava.awt.headless=true \
-Dfile.encoding=UTF-8 \
-Duser.timezone=Asia/Shanghai \
-Djava.net.preferIPv4Stack=true \
-jar target/seckill-0.0.1-SNAPSHOT.jar \
--server.port=8080
```


如果在一个机器启动多实例需要修改--server.port=以避免端口冲突

## sql

```bash
-- 删除seckill_order表的联合唯一索引用于测试一人可以下多单
ALTER TABLE seckill_order DROP INDEX u_userid_goodsid;
```

```bash
update seckill_goods set start_date=CURDATE();
update seckill_goods set end_date=date_add(start_date, interval +2 day);

update seckill_goods set stock_count=100;
delete from order_info;
delete from seckill_order;



select * from seckill_goods where id =1;
select count(*) from order_info;
select count(*) from seckill_order;


select stock_count from seckill_goods where id =1 
union all
select count(*) from order_info 
union all
select count(*) from seckill_order;


select * from red_packet_info where red_packet_id=1;
select sum(amount) from red_packet_record where red_packet_id=1;
select * from red_packet_record where red_packet_id=1;

```

## api

### 秒杀API

```bash
============================(非一人一单、数据库操作)=========================================
## 非一人一单，无乐观锁，无悲观锁
http://127.0.0.1:8080/api/v1?goods_id=1&user_id=1410080408

## 非一人一单，乐观锁 where条件 库存=旧库存 成功率低
http://127.0.0.1:8080/api/v2?goods_id=1&user_id=1410080408

## 非一人一单，乐观锁 where条件 库存>0
http://127.0.0.1:8080/api/v3?goods_id=1&user_id=1410080408

## 非一人一单，悲观锁
http://127.0.0.1:8080/api/v4?goods_id=1&user_id=1410080408

==============================(一人一单、数据库操作)=======================================
## 一人一单，无乐观锁，无悲观锁
http://127.0.0.1:8080/api/one/v1?goods_id=1&user_id=${user_id}

## 一人一单，乐观锁 where条件 库存=旧库存 成功率低
http://127.0.0.1:8080/api/one/v2?goods_id=1&user_id=${user_id}

## 一人一单，乐观锁 where条件 库存>0
http://127.0.0.1:8080/api/one/v3?goods_id=1&user_id=${user_id}

## 一人一单，悲观锁
http://127.0.0.1:8080/api/one/v4?goods_id=1&user_id=${user_id}

==============================(一人一单、前置reids预减库存)===================================
http://127.0.0.1:8080/api/redis_one/v1?goods_id=1&user_id=${user_id}
http://127.0.0.1:8080/api/redis_one/v2?goods_id=1&user_id=${user_id}
http://127.0.0.1:8080/api/redis_one/v3?goods_id=1&user_id=${user_id}
http://127.0.0.1:8080/api/redis_one/v4?goods_id=1&user_id=${user_id}
## 以上 v1-v4 实现逻辑一样了
==============================(清理数据)===================================
http://127.0.0.1:8080/api/clean
```
### 红包API

```bash
## 清理数据并创建红包

http://127.0.0.1:8080/api/red/clean?red_packet_id=1&total_amount=10000&total_packet=50

## 抢红包API(数据库乐观锁)
http://127.0.0.1:8080/api/red/occ?red_packet_id=1&user_id=${user_id}
```

## tables

#### goods

| 序号 |       名称       |      描述       |      类型       | 默认值  |
|:--:|:--------------:|:-------------:|:-------------:|:----:|
| 1  |      `id`      |               |  bigint(20)   |      |
| 2  |  `goods_name`  |     商品名称      |  varchar(16)  |      |
| 3  | `goods_title`  |     商品标题      |  varchar(64)  |      |
| 4  |  `goods_img`   |     商品图片      |  varchar(64)  |      |
| 5  | `goods_detail` |    商品介绍详情     |   longtext    |      |
| 6  | `goods_price`  |     商品单价      | decimal(10,2) | 0.00 |
| 7  | `goods_stock`  | 商品库存，-1表示没有限制 |    int(11)    |  0   |
| 8  | `create_date`  |               |   datetime    |      |
| 9  | `update_date`  |               |   datetime    |      |

#### seckill_goods

| 序号 |       名称       |  描述  |      类型       | 默认值 |
|:--:|:--------------:|:----:|:-------------:|:---:|
| 1  |      `id`      |      |  bigint(20)   |     |
| 2  |   `goods_id`   | 商品id |  bigint(20)   |     |
| 3  | `seckil_price` | 秒杀价  | decimal(10,2) |     |
| 4  | `stock_count`  | 秒杀数量 |    int(11)    |     |
| 5  |  `start_date`  |      |   datetime    |     |
| 6  |   `end_date`   |      |   datetime    |     |


#### order_info

| 序号 |       名称        |                   描述                    |      类型       | 默认值 |
|:--:|:---------------:|:---------------------------------------:|:-------------:|:---:|
| 1  |      `id`       |                                         |  bigint(20)   |     |
| 2  |    `user_id`    |                  用户id                   |  bigint(20)   |     |
| 3  |   `goods_id`    |                  商品id                   |  bigint(20)   |     |
| 4  |    `addr_id`    |                 收货地址id                  |  bigint(20)   |     |
| 5  |  `goods_name`   |                冗余过来的商品名称                |  varchar(16)  |     |
| 6  |  `goods_count`  |                  商品数量                   |    int(11)    |     |
| 7  |  `goods_price`  |                  商品价格                   | decimal(10,2) |     |
| 8  | `order_channel` |        支付通道：1 PC、2 Android、3 ios        |    int(2)     |  0  |
| 9  |    `status`     | 订单状态：0 未支付，1已支付，2 已发货，3 已收货，4 已退款，5 已完成 |    int(2)     |     |
| 10 |  `create_date`  |                                         |   datetime    |     |
| 11 |   `pay_date`    |                  支付时间                   |   datetime    |     |


#### seckill_order

| 序号 |     名称     | 描述 |     类型     | 默认值 |
|:--:|:----------:|:--:|:----------:|:---:|
| 1  |    `id`    |    | bigint(20) |     |
| 2  | `user_id`  |    | bigint(20) |     |
| 3  | `order_id` |    | bigint(20) |     |
| 4  | `goods_id` |    | bigint(20) |     |


#### user

| 序号 |        名称         | 描述 |     类型      | 默认值 |
|:--:|:-----------------:|:--:|:-----------:|:---:|
| 1  |       `id`        |    |   int(11)   |     |
| 2  |    `user_name`    |    | varchar(45) |     |
| 3  |      `phone`      |    | varchar(11) |     |
| 4  |    `password`     |    | varchar(65) |     |
| 5  |      `salt`       |    | varchar(45) |     |
| 6  |      `head`       |    | varchar(45) |     |
| 7  |   `login_count`   |    |   int(11)   |     |
| 8  |  `register_date`  |    |  datetime   |     |
| 9  | `last_login_date` |    |  datetime   |     |

#### red_packet_info
红包信息表，新建一个红包插入一条记录

| 序号 |         名称         |     描述     |        类型        |         默认值          |
|:--:|:------------------:|:----------:|:----------------:|:--------------------:|
| 1  |        `id`        |            |     int(11)      |                      |
| 2  |  `red_packet_id`   |    红包id    |    bigint(20)    |          0           |
| 3  |   `total_packet`   |   红包总个数    | int(11) unsigned |          0           |
| 4  |   `total_amount`   | 红包总金额，单位分  | int(11) unsigned |          0           |
| 5  | `remaining_amount` | 剩余红包金额，单位分 | int(11) unsigned |          0           |
| 6  | `remaining_packet` |   剩余红包个数   | int(11) unsigned |          0           |
| 7  |       `uid`        | 创建建红包的用户标识 |    bigint(20)    |          0           |
| 8  |   `create_time`    |    创建时间    |   datetime(6)    | CURRENT_TIMESTAMP(6) |
| 9  |   `update_time`    |    更新时间    |   datetime(6)    | CURRENT_TIMESTAMP(6) |
| 10 |     `version`      |  版本号用于乐观锁  |     int(11)      |          0           |


#### red_packet_record
抢红包记录表，抢一个红包插入一条记录

| 序号 |       名称        |     描述      |        类型        |         默认值          |
|:--:|:---------------:|:-----------:|:----------------:|:--------------------:|
| 1  |      `id`       |             |     int(11)      |                      |
| 2  |    `amount`     | 抢到红包的金额，单位分 | int(11) unsigned |          0           |
| 3  |   `nick_name`   | 抢到红包的用户的用户名 |   varchar(32)    |          0           |
| 4  |    `img_url`    | 抢到红包的用户的头像  |   varchar(255)   |          0           |
| 5  |      `uid`      | 抢到红包用户的用户标识 |    bigint(20)    |          0           |
| 6  | `red_packet_id` |    红包id     |    bigint(20)    |          0           |
| 7  |  `create_time`  |    创建时间     |   datetime(6)    | CURRENT_TIMESTAMP(6) |
| 8  |  `update_time`  |    更新时间     |   datetime(6)    | CURRENT_TIMESTAMP(6) |



## ref

- <https://github.com/MrSorrow/seckill>
- <https://github.com/hfbin/Seckill>
- <https://github.com/CocaineCong/Go-SecKill>
- <https://github.com/dyrnq/heima-dianping-redis>
- <https://github.com/TaXueWWL/seckill-rocketmq>
- <https://github.com/sx89/seckill-practice>